-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 18, 2020 at 11:27 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `graphql2`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `user_id`, `title`, `content`, `created_at`, `updated_at`) VALUES
(1, 1, 'Sed eaque reiciendis doloremque dolorem assumenda sed cum.', 'Qui repellendus at assumenda eaque rerum dolorem. Aut occaecati in tempore minima sint. Ipsam vel voluptas quia aperiam.\n\nQuos et natus eius quibusdam cupiditate ut voluptas. Ipsum et dicta aliquid nostrum incidunt soluta adipisci ex. Ducimus similique facilis sunt. Est voluptate id optio enim provident deserunt et.\n\nNihil et voluptatibus debitis perspiciatis reiciendis occaecati reprehenderit. Perferendis accusantium iusto illum deleniti temporibus quaerat id.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(2, 1, 'Illum esse alias molestias quam repellendus porro eligendi.', 'Iusto qui facere molestiae sit. Aliquam voluptate consequuntur rerum blanditiis fugit molestiae blanditiis rem. Occaecati iure reprehenderit vel. Harum in ut fugit nihil in cumque nemo necessitatibus.\n\nQuia ut eum id reprehenderit. Voluptatem animi cumque dicta esse doloremque excepturi. Aut quasi praesentium et repellat inventore quae. Laboriosam et deserunt nisi quos delectus illum est.\n\nVelit distinctio dolores voluptatum minima. Earum voluptatem blanditiis sed ex aut et.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(3, 1, 'Esse impedit quia dolor odio sed maxime.', 'Vero qui nostrum accusamus quia quae commodi alias. Dolores ab aut sed qui officiis nemo vitae. Quae eaque similique fuga qui ex sunt.\n\nSunt sit facere et fugit perferendis rerum. Odit officiis nihil quasi aliquid voluptatem ipsam veniam molestias.\n\nVoluptas quos vel dolores dignissimos saepe. Minus est quia quo culpa repellendus dolorem aut dolores. Officia qui et nesciunt reiciendis totam autem adipisci. Nisi aperiam dolorem dolores et sed. Voluptatum maiores iusto aut eligendi rem.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(4, 1, 'Ratione non similique accusantium modi et sit repellendus.', 'Id reiciendis rerum aut unde voluptatibus corporis et. Sit molestiae adipisci fugit id et. Corrupti quis aut autem voluptatibus et dolorem ea vel.\n\nEt est quia modi rerum. Suscipit nemo quaerat architecto. Est nam optio sit sunt.\n\nOptio libero enim tempore cupiditate. Laborum ut ea est nam veniam esse. Quo nesciunt perferendis autem consequatur sed. Aut ut corporis consequatur ab.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(5, 1, 'Repudiandae voluptatibus omnis sit quo.', 'Dolorum in saepe deleniti a. Aspernatur soluta aut aut fuga perspiciatis eaque autem. Minima aut eos quas officiis officia nihil. Sequi ut omnis maxime fugit rerum.\n\nTempore laboriosam voluptas veniam qui aliquam. Autem quos iusto commodi mollitia consequuntur reiciendis numquam. Aliquid et autem aut et ullam. Tenetur praesentium adipisci sunt aut atque earum.\n\nLaudantium qui quia maiores est. Et dignissimos nihil amet. Minima voluptas dicta fuga et dignissimos. Voluptas nisi eius sequi optio repellendus.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(6, 2, 'Vitae non sit commodi.', 'Qui maxime rem et non culpa reprehenderit magni. Fuga voluptas id quos iure. Id cupiditate inventore quo nobis ea nobis dolorem. Dolor nihil illo autem perspiciatis quia officiis omnis.\n\nIpsa et perferendis sed a. Enim in veniam et cumque placeat in deserunt expedita. Perspiciatis consectetur repudiandae quo corporis. Consectetur ipsum et nesciunt neque impedit eaque dolor.\n\nTempora quia corrupti et voluptatem ut vero tempora ullam. Sapiente aut nisi laudantium sunt sed minus eum. Ut optio autem provident et tempore hic sit. Numquam eum mollitia non doloremque praesentium.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(7, 2, 'Vero et dicta dolorem tempore ipsam officia.', 'Sed in similique asperiores qui nostrum. Voluptas eveniet et temporibus debitis dolores. Minima id eius facere libero.\n\nEveniet modi numquam et et quod illo quia numquam. Id reiciendis rerum explicabo eveniet porro repellat excepturi. Id ut et dolorem impedit sunt.\n\nQuia maxime deserunt porro est fugiat incidunt voluptatem. Quibusdam asperiores eum quia ex inventore atque dolorum. Cupiditate molestiae sapiente dicta. Voluptatem ad atque tenetur eos provident iusto.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(8, 2, 'Ea exercitationem non omnis iste officiis et.', 'Consequatur aut totam ut enim. Nemo maxime est doloribus voluptas ea. Corporis quia ut sequi repellat delectus. Quo dicta eum delectus.\n\nExcepturi ut qui aperiam autem dolores et. Voluptas officiis qui et reiciendis consectetur culpa mollitia. Eum dolorum sunt eum.\n\nEst labore corrupti placeat hic ea. Rem et sit quod dolor blanditiis expedita qui aliquam. Corrupti eos earum magni fuga repellendus ratione. Rerum et exercitationem quod a explicabo. Voluptatem porro sint error nam.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(9, 2, 'Animi unde dolorem occaecati soluta fuga.', 'Dignissimos cum dolore ipsa aut corrupti quas molestiae sed. Perferendis deleniti molestiae iste veniam doloribus. Iste autem architecto molestiae voluptates. Dolorum voluptate nesciunt sed dicta ipsum error voluptas dolore.\n\nUt sint nemo soluta et aliquam. Accusamus voluptate molestias aspernatur explicabo a dolore. Sapiente odit vel nam doloribus. Porro quibusdam velit officiis maxime perferendis.\n\nVoluptatem praesentium dolorem sint quia. Molestiae ad voluptatem eaque numquam blanditiis in vel eos. Qui magni culpa a sint ut quae quis. Voluptate eligendi perspiciatis in et. Nisi non cupiditate ipsum praesentium.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(10, 2, 'Soluta libero quasi voluptatem quia mollitia velit.', 'Porro alias nesciunt nesciunt dignissimos. Repellendus qui quia voluptas sit. Nulla omnis velit dignissimos et quis qui. Eum inventore culpa et et est rerum quia.\n\nMagni qui repudiandae culpa. Veritatis aliquam assumenda magni. Est maiores voluptatem sit consequatur facere. Sunt qui consequatur sed et et.\n\nEst cum porro aspernatur est repellat inventore optio. Excepturi cum dolorum doloribus nesciunt natus expedita sit. Eum nostrum odit quam ipsa vero error possimus.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(11, 3, 'Sit et sapiente officiis neque minima.', 'Sit consequatur alias earum rerum aut voluptate tenetur. Quia dolor architecto sit necessitatibus aliquid sunt non. Voluptas sit quisquam aut mollitia. Fugit molestiae in ab quisquam odio. Laborum et qui non eum explicabo neque et doloribus.\n\nIn mollitia est quo neque est quam. Aliquam nobis ut ducimus dicta voluptatum nihil omnis. Non natus ipsum ut qui et pariatur. Non dolores impedit corporis. Aliquam eos sequi voluptas fuga alias aperiam non.\n\nEst sed quas est culpa eos mollitia. Sit sint deserunt quam ipsam debitis ad non.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(12, 3, 'Repudiandae laboriosam sit laborum fuga.', 'Consectetur reiciendis sunt natus exercitationem. Aut eveniet ut adipisci eveniet amet neque. Ipsum labore perferendis sed enim voluptatem sit ab.\n\nPorro at alias eius. Ut saepe ea ipsa et. Voluptatem nihil sunt delectus quia optio hic. Placeat saepe accusamus nihil omnis aut.\n\nSoluta dolorum atque et. Consequatur impedit non dolores omnis dolorem. Praesentium sed dolore qui voluptates.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(13, 3, 'Sapiente pariatur dolorum dolor molestiae est qui autem.', 'Laboriosam pariatur qui sed. Quam voluptatem consectetur nisi commodi nesciunt dolor.\n\nQui totam non et assumenda consequatur. Dignissimos quidem ex pariatur iusto expedita autem nemo. Sit temporibus officia ut corporis fuga voluptatem.\n\nSunt quis voluptas quia consequatur odit ut. Repellat molestias sint pariatur unde. Aperiam aut quia veritatis ut. Aspernatur explicabo hic molestias.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(14, 3, 'Sint iste est accusamus expedita aut consequatur.', 'Dignissimos est dignissimos atque sed ut quam. Aspernatur sequi qui id.\n\nFuga doloremque blanditiis cupiditate voluptas neque omnis rerum nemo. Consectetur quisquam amet deleniti et dolor ut repudiandae. Illum cumque id qui sed. Dolore nemo necessitatibus a ipsum maxime corporis.\n\nQuis voluptates illo ut velit alias. Nesciunt dolores deleniti ea blanditiis libero. Numquam laudantium cupiditate ipsum aperiam laborum dolorem dolore. Inventore facere voluptatem est ut molestiae ipsam et.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(15, 3, 'Vel repellendus ullam magnam omnis.', 'Eveniet sit ullam inventore. Dolores a sint reprehenderit illo soluta. Et sint et modi consequatur dolores cupiditate.\n\nSed commodi eos et et soluta laudantium. Perferendis voluptatem facere repellat quia. Aut mollitia aspernatur cupiditate provident consectetur atque. Voluptatibus commodi illum unde officiis sunt.\n\nTempora neque illum magnam repellat. Fuga impedit fugit possimus expedita magni deserunt officia. Facilis ut blanditiis beatae consequatur incidunt veniam tempora cumque.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(16, 4, 'Ab veritatis quia delectus officia et dicta sapiente.', 'Exercitationem voluptas id quisquam autem maxime suscipit qui. Est qui at labore. Asperiores est nobis natus minus et ut. Qui facilis soluta voluptate non.\n\nReprehenderit sequi maxime id quod et corporis provident. Quasi amet dolor harum deleniti quia dolor optio. Perferendis modi et incidunt aliquam. Nostrum ipsa fugiat et commodi consequuntur tempore. Animi doloribus velit architecto voluptas quis qui molestias.\n\nDolor occaecati nesciunt velit cupiditate maiores omnis. Et exercitationem sint laboriosam quisquam natus laboriosam sed culpa. Magnam sed autem qui aliquam.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(17, 4, 'Odit corrupti rerum dignissimos nemo dolorem ipsam officiis illo.', 'Minus mollitia laborum officia eaque officiis voluptatem. Ab consequuntur aliquam consectetur consequatur rerum. Accusantium ut omnis cumque. Aliquam rem officia ducimus dignissimos quaerat.\n\nIusto sint nobis tempora aut ipsa aspernatur molestiae. Quia qui laboriosam blanditiis laborum quasi cupiditate aliquam quis. Tenetur deleniti illo non aliquid quia. Soluta accusantium aut sunt et qui. Aut esse mollitia et.\n\nNumquam ipsa quo non quisquam. Et beatae consequatur officia aut optio. Eos atque quaerat alias amet blanditiis non.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(18, 4, 'Aut sit eos libero nostrum et ea.', 'Qui voluptas hic sunt ea in distinctio magnam. Quas illum praesentium aspernatur repellat temporibus accusantium fugiat. Delectus aut vero et eveniet officiis. Laboriosam voluptatem soluta sed et facere. Enim deserunt iste ab est.\n\nA autem totam quas ratione laudantium qui. Quod inventore consequatur iste. Culpa fugit et ut. Accusantium exercitationem expedita enim assumenda et voluptates.\n\nRatione qui doloribus iusto ab modi assumenda possimus. Perferendis minima et debitis. Sed pariatur qui temporibus dolores. Aliquid facere enim qui similique aut sint sed.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(19, 4, 'Repudiandae iusto et nam.', 'Quaerat molestias vero nulla reprehenderit autem modi dolores. Tenetur temporibus consequatur officiis qui rem velit. Possimus qui porro iste maxime et. Nemo sed laborum qui facere omnis quo.\n\nUt at at nisi debitis dolores exercitationem. Veniam consequatur aut laboriosam labore. Distinctio voluptas tempore fugit praesentium cumque aut illo.\n\nPariatur doloribus dolorum itaque eum velit ea. Vel harum facere possimus ea. Qui nihil ab repellat porro necessitatibus.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(20, 4, 'Porro occaecati qui ut deserunt.', 'Culpa cum autem blanditiis voluptatem ducimus. Eum dolor nam expedita sunt eum iusto. Accusantium sunt sit nihil ad saepe dolorem sapiente sint. Quis doloremque ad amet magni inventore nesciunt et. Qui illum ratione labore nobis necessitatibus natus architecto.\n\nCumque voluptas voluptatibus id. Nesciunt sapiente totam voluptatem voluptatem similique. Dignissimos iure voluptates qui consequatur ea veniam et accusantium.\n\nLabore mollitia sint quo repellendus dignissimos. Enim repudiandae aspernatur cum aut. Magnam possimus omnis qui sed odio vitae et. Libero at odio quibusdam at recusandae aut.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(21, 5, 'Commodi sed eligendi amet non error.', 'Reprehenderit voluptatum blanditiis dicta natus. Iste odit error delectus debitis sed. Ut quaerat voluptatum debitis dolor consequatur. Eius voluptatem voluptas eveniet rem animi.\n\nRepellat quae dolorem autem voluptas sapiente impedit incidunt. Rerum placeat dolorem quidem. Sit maxime voluptatem sunt dolores enim. Blanditiis et cum laboriosam sunt excepturi impedit tempore. Neque vel omnis odit qui quas quod.\n\nIusto sint voluptas excepturi voluptatem dolores. Voluptas delectus ipsa qui provident autem reprehenderit ut. Aliquam ut totam esse quia ad non quidem et. Molestiae dolores quas ex sint sed.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(22, 5, 'Veniam commodi ad unde expedita necessitatibus quod.', 'Aut fugit asperiores maiores commodi perferendis dolor. Itaque repellendus veritatis non a repellat non delectus. Sint impedit maiores et fugiat error eveniet.\n\nDelectus velit dolorum similique vero rerum. Tempora ut optio libero. Consectetur omnis aut sed corporis excepturi. Vitae velit voluptatum et modi reiciendis.\n\nSunt et enim sit maiores molestias maxime consequatur. Sit quia pariatur impedit iure voluptatem consequatur nihil. Et distinctio voluptas rerum saepe dolor. Voluptatum sapiente at adipisci rerum vel beatae repellendus sunt.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(23, 5, 'Rem ut esse quis optio rerum blanditiis consectetur.', 'Molestiae cumque dolorem earum cum. Tenetur qui quod iure aperiam est aliquid cumque pariatur. Qui eius minima provident cum.\n\nNulla maiores magni quis voluptas excepturi. Iste labore magnam fugiat voluptatibus cum placeat. Minus id beatae dolorem saepe cumque.\n\nRepudiandae ut enim soluta qui eveniet at quia. Ut placeat minus voluptas qui. Maxime omnis nisi ea dolorem ab eos. Dolores eos perspiciatis odio voluptatum dolorem iusto.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(24, 5, 'Corrupti assumenda placeat est eum assumenda.', 'Et est repudiandae dolor et qui explicabo molestiae molestias. Sit non hic in minima. Repellendus libero enim praesentium quae.\n\nRepellat animi quia vero rerum eos est omnis. Sit tenetur aut laudantium delectus consequatur. Consequatur delectus voluptatem magnam nostrum.\n\nFugit non ipsum accusantium laborum commodi odio earum. Voluptatem soluta aperiam sed corporis qui. Nihil natus laboriosam corporis assumenda ipsa odit et.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(25, 5, 'Quibusdam maxime nulla quis sit quia.', 'Animi atque ut voluptas ut voluptatem qui at. Ipsum ut ratione nihil molestiae. Non illo sint sunt rem eveniet.\n\nFacilis quae nam alias officiis ab perferendis. Accusantium non et aliquam cupiditate autem perspiciatis.\n\nExercitationem tenetur qui cupiditate eum omnis. Mollitia sint aut iusto molestias accusamus odio cumque.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(26, 6, 'Enim sed voluptas sunt porro amet consequuntur.', 'Et aut harum quia. Id tempore quia reiciendis aut debitis error laboriosam. Eos dignissimos suscipit doloribus veritatis unde.\n\nPraesentium nulla earum reiciendis accusantium vero in officiis. Non consequatur voluptatum dolor maxime. Id debitis nihil maxime eum aut nemo quo. Dolorum et molestiae inventore non saepe.\n\nAliquid provident pariatur tempore porro et voluptas commodi. Placeat minima consequatur doloremque excepturi sed laudantium. Omnis dolore consequuntur dignissimos.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(27, 6, 'Reprehenderit id non voluptatibus rerum ad expedita.', 'Cumque odio nesciunt sunt tenetur voluptatem corporis quae. Facilis voluptatem officia minus sit mollitia est amet.\n\nVeritatis odit similique aliquam aut aut amet. Repudiandae quia consequuntur modi debitis impedit cupiditate ducimus. Aut esse quo harum dolor sapiente omnis. Numquam et laborum itaque.\n\nNihil maxime et iste temporibus eligendi illum. Placeat sunt neque reprehenderit aut consequatur facilis. Rerum dolorum alias ut vel eos. Assumenda velit harum dolorem minus impedit ad.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(28, 6, 'Modi debitis ut aliquid earum ut a.', 'Saepe quae officia ut id qui dolorem assumenda. Maxime nisi officia accusantium est voluptatem. Ad qui expedita consequuntur modi enim.\n\nQuis animi nisi molestiae voluptatum facere repudiandae. Dolorem ea vitae quam voluptate nihil et voluptatibus. Modi accusamus eos doloremque minima. Aut provident recusandae et accusantium sed et.\n\nQui eligendi magnam architecto nulla impedit. Esse amet molestiae officia ipsam.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(29, 6, 'Ab dignissimos saepe quod adipisci.', 'Dolore dignissimos laudantium illum sunt. Non mollitia voluptatem mollitia soluta inventore praesentium distinctio. Dolorum minus perferendis quisquam et.\n\nNihil omnis iusto omnis pariatur quae quia. Laboriosam natus consequuntur ut omnis. Aliquam facilis voluptatem unde optio voluptatum rerum earum.\n\nQuos tempore consequatur inventore et vero. Sit eaque beatae cumque ut praesentium ullam cumque.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(30, 6, 'Id ut sed iusto officiis fuga quaerat natus.', 'Est ut quibusdam eos labore ea delectus. Est aliquam accusamus eius voluptate quo reprehenderit cupiditate. Odio molestias suscipit laboriosam maxime aspernatur sit corrupti eius.\n\nCupiditate id neque consequatur consequatur. Ut ad quidem explicabo suscipit necessitatibus hic soluta. Laudantium odio autem unde et adipisci itaque dolor. Voluptas quis autem dolore sequi est.\n\nQuos velit eum hic repellendus voluptatem nobis nobis. Pariatur architecto eius quisquam commodi. Reprehenderit voluptate aliquam beatae explicabo dolor adipisci nobis. Amet explicabo facilis impedit ut.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(31, 7, 'Sed aut atque doloribus facere sit veritatis tenetur.', 'Nihil reprehenderit quia sapiente beatae. Et labore dolore repellat harum perferendis qui et iste. Quas consequuntur corporis reprehenderit aut. Est soluta aut magni fugiat ut dolores.\n\nMagnam qui aut voluptatum voluptatem quis perferendis. Esse repudiandae fugiat nihil eos rem aut. Omnis esse veritatis quam recusandae ad. Consequatur praesentium nam quia et qui vero velit.\n\nEos ut ut soluta aut fugiat libero. Hic a rerum velit nesciunt sequi. Eum praesentium aut adipisci libero. Quas id iusto est facilis debitis magnam asperiores velit.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(32, 7, 'Sapiente eveniet dolore doloribus sed explicabo est.', 'Aliquid sit harum expedita officia minima. Sint est optio laudantium est non in. Aliquam et eius corporis nemo. Error quo nisi similique vitae facere distinctio.\n\nNihil dolores et accusamus. Ut ex ut esse ut at aut id. Ullam ipsam autem consequatur aliquid eveniet reprehenderit. Ipsa reprehenderit aliquam ut asperiores voluptates natus.\n\nUt eum est reprehenderit officiis. Debitis qui voluptates delectus itaque. Alias esse esse accusamus sint. Dicta unde deleniti voluptatem voluptate sed.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(33, 7, 'Et beatae id blanditiis sunt.', 'Quo error aliquid nemo illo. Dolorum inventore quia amet facilis laborum necessitatibus ipsa. Qui dolor enim et quae maxime molestias quo.\n\nQui sint provident magnam ratione minus. Beatae dignissimos ex eius recusandae fugit dolorum iste. Commodi ex quia dolor error voluptas earum. Enim non sed occaecati sunt.\n\nModi quisquam enim consequatur harum labore dolor. Aut est nulla voluptatem voluptatum dignissimos sunt. Officia consequuntur eveniet doloremque omnis quia beatae. Deleniti consequatur quis perferendis labore provident nulla. Cumque dolore consectetur sed.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(34, 7, 'Aut adipisci libero quaerat reprehenderit aperiam ea delectus.', 'Animi doloribus culpa culpa tempora. Ex voluptas illo quam. Perspiciatis hic id recusandae. Mollitia consequatur rerum ullam et tempore.\n\nEos ullam dicta ut in qui enim. Alias eos eius et odit porro et veritatis. Accusantium qui ut dolorem sequi.\n\nAliquid et saepe et ducimus corporis. Hic quaerat minus earum aspernatur voluptatem. Similique nisi velit sequi. Dignissimos quis velit inventore voluptatum laborum voluptas voluptas.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(35, 7, 'Et culpa consequatur non aliquam eveniet alias autem.', 'Unde occaecati excepturi repellat dolorum nemo. Commodi sunt omnis dolorum. Asperiores consequatur vero deserunt nihil temporibus dolores omnis.\n\nQuis et ea recusandae praesentium autem. Nulla rerum qui blanditiis et molestiae. Quas eveniet eos officia rerum aliquam doloremque.\n\nDolore est debitis id natus sunt aut. Rerum nesciunt sed fugiat cumque quasi ut eum. Numquam quos exercitationem itaque iusto aperiam aut.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(36, 8, 'Rerum id itaque veritatis.', 'Ea recusandae placeat autem voluptas. Corrupti qui et in. Enim sit non commodi maiores voluptatum.\n\nEos in consequatur et rerum omnis dolorem aut. Exercitationem quod tenetur doloribus sed itaque ipsa et. Dignissimos nemo repudiandae non dolorem. Facilis et occaecati iste consequuntur dicta.\n\nRerum est ea et ipsum itaque animi molestiae culpa. Qui quasi ipsum dicta ut. Aperiam commodi nesciunt accusantium perferendis repellat a cupiditate. Laudantium velit ea perspiciatis.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(37, 8, 'Officia ratione vel at culpa laborum ea debitis sit.', 'Voluptates qui nulla consequatur nam fugit omnis. Optio saepe quidem qui et officiis ut. Iusto ea quod deleniti dignissimos sint. Eum sit et omnis perspiciatis ullam ut.\n\nExercitationem tenetur et quam. Voluptates aperiam praesentium architecto quod aliquam. Odit est consequatur architecto est.\n\nEum aut et vel et est quia aliquid. Repellendus reprehenderit et dolor voluptatem id ad dolores. Voluptas placeat vel alias. Quam sed quod assumenda deleniti dolorum est ipsam. Eius suscipit qui ut odit.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(38, 8, 'Magnam ipsum et voluptatibus voluptatem rem delectus atque.', 'Molestiae sit voluptatem qui ex corrupti quaerat possimus. Quia sunt beatae sunt veritatis quaerat qui earum. Aut et eum ut occaecati nisi quisquam.\n\nExcepturi voluptas nam deserunt minima vel totam qui inventore. Vitae sed veritatis quo eos. Est debitis ab temporibus dicta. Vero veniam est repudiandae dolore labore et.\n\nAnimi nobis et sit quam. Molestiae sequi doloribus neque quos et sint deleniti.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(39, 8, 'Consequuntur perspiciatis porro repellat qui impedit.', 'Eveniet eum qui ipsam sed ut harum hic. Ut maiores non tempora. Nemo rerum ea voluptatem est hic. Corporis aliquid nisi assumenda aut voluptas cumque.\n\nIllo repudiandae deleniti assumenda non iure a consequuntur. Architecto veniam quis repellat. Est minus error aut alias provident ipsum.\n\nRepellendus porro labore laboriosam qui iure ipsa harum eos. Ea perspiciatis officiis est assumenda. Aliquid odio at nostrum non totam. Cumque inventore perferendis sunt sed aut.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(40, 8, 'Ut sed consectetur minus consequuntur illo.', 'Magnam sit a tenetur. Voluptas perspiciatis aut repellendus minima saepe possimus asperiores. Voluptatem aut eos eveniet qui sapiente aliquam. Reprehenderit quas ullam quia similique.\n\nDicta sunt saepe assumenda. Commodi ex recusandae eius perspiciatis ea quia placeat atque. Non a harum earum iure recusandae eveniet necessitatibus.\n\nConsectetur quibusdam ipsum minima. Atque nihil ut repellat sit quisquam officiis eum fugiat. Est hic soluta impedit sunt voluptatum.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(41, 9, 'Eius assumenda qui at possimus enim minima.', 'Aspernatur in architecto autem. Odit voluptatum sequi suscipit quia numquam. Totam adipisci eos magnam est fuga dicta.\n\nQuia est tenetur consequatur et ea. Dolore earum ab quia voluptates. Dolor placeat ea maxime excepturi.\n\nRepellat ullam laudantium tempore. Blanditiis eaque totam omnis. Tempore harum id sunt eius voluptatum.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(42, 9, 'Iusto nihil quia odio ipsa.', 'Nihil voluptate velit fuga voluptas quis ut vero. Sint ratione tenetur temporibus quaerat. Soluta rem nisi inventore adipisci ea rerum non excepturi. Velit culpa ut ipsam minima ut iure.\n\nVoluptatem dolorum aut impedit quaerat delectus qui eveniet. Necessitatibus eos perspiciatis quis sunt totam nulla at. Sed voluptatem non omnis et rem nihil aut reprehenderit. Maiores rerum et dolores perferendis dolores explicabo quis ullam.\n\nDeserunt eos quisquam aut voluptatem perspiciatis mollitia. Id et id vel recusandae nobis. Eaque qui reiciendis fugit et et.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(43, 9, 'Eveniet et quia sint pariatur tempora eligendi occaecati.', 'Exercitationem eveniet sunt ut officiis error illum aperiam. Nesciunt aut molestiae qui quod. Ipsa dolor non culpa ut. Beatae beatae enim cupiditate vero commodi explicabo.\n\nDeleniti nulla qui eaque non quibusdam sapiente. Sit sint modi ea ut sed voluptate asperiores. Quos asperiores amet necessitatibus dolore maxime quasi.\n\nExplicabo atque necessitatibus in facilis. Quo aliquid fugiat consequatur maxime provident qui nam. Et culpa ab ipsum. Ut exercitationem maxime sit consequatur eligendi neque sit non.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(44, 9, 'Vero deleniti eos et distinctio aut sint.', 'Distinctio vitae rerum adipisci hic. Sunt nam illo impedit sint. Eaque odit voluptatem officia in soluta voluptas commodi.\n\nFuga quam suscipit rem est sit sed enim. Rerum repellendus soluta qui fuga. Eligendi cum sed sit consequatur nobis velit dolor.\n\nLaudantium explicabo alias nihil sit non reiciendis alias. Velit error at soluta ipsa. Cum minima quos sint iusto. Similique quaerat error molestias alias quaerat est.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(45, 9, 'Ut repudiandae voluptatem saepe sunt.', 'Voluptas odit sed magni expedita magnam numquam et. Aperiam repellat ad nihil sit tempore sunt. Quisquam in sed illum quia.\n\nReprehenderit a esse dolores non. Quam laboriosam tempore vero accusantium. Qui omnis eum asperiores blanditiis eum qui. Qui perferendis nemo quam quod.\n\nNihil ut et voluptatum doloremque sed. Eos eos ipsam quod fugit eum aut. Ipsa doloremque consectetur nostrum quis explicabo et.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(46, 10, 'Non nihil cumque et ab voluptatum sed sint maxime.', 'Dolor molestias est magni et. Aut dolorum assumenda iste. Maxime amet non rem enim aperiam fugit iste.\n\nEa reprehenderit nam itaque. Molestiae cum harum quis ipsa molestiae qui est sed. Quia iure odio sed reiciendis quia fugiat et. Ullam dolor id cupiditate et ratione qui nemo aut. Aliquid autem officiis perferendis sed veritatis quasi occaecati.\n\nQui sint enim quia eum. Quo blanditiis aut laborum sit. Ea ipsa nobis ipsa vel sit et. Consectetur architecto porro debitis deserunt sed. Voluptatum doloribus autem necessitatibus voluptas at.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(47, 10, 'Quis sit exercitationem consequatur omnis at.', 'Modi ex officia cupiditate. Neque at sint ut perspiciatis sed quasi. Voluptas vel perspiciatis voluptatem dolores. Nisi sit magnam consequuntur ut dolores. Et velit pariatur iure eius minima aut voluptatem.\n\nVeritatis et quis voluptate numquam minima dignissimos. Quaerat autem in aperiam aspernatur praesentium. Et quo perferendis quod provident.\n\nAut fugit voluptas laudantium eligendi. Hic ex reprehenderit fuga voluptatum. Esse quam sed beatae mollitia quibusdam. In libero voluptate et et.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(48, 10, 'Nostrum suscipit quo praesentium voluptas suscipit.', 'Error qui placeat ipsa quidem id id voluptates. Commodi amet quam similique quia a voluptatem ipsam hic. Iusto eius sit quidem nihil ea officia ex.\n\nQuis eum totam est modi veritatis corrupti eum. Atque non est qui. In eligendi quia ut deleniti asperiores.\n\nEligendi dignissimos nam sed quia facilis et. Voluptatum libero dolorem facilis omnis. Veniam tenetur voluptas voluptatibus inventore et.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(49, 10, 'Nobis tenetur veniam molestiae id.', 'Ullam at libero non aliquam aperiam accusantium et aut. Commodi et ipsam voluptatibus magnam. Nemo officia magnam harum et. Voluptas cumque voluptatem accusamus optio atque praesentium.\n\nOdio itaque laudantium iste sunt ad id iste labore. Dolor porro eos quae libero beatae ut. Qui consequatur enim est dolorum natus deserunt laudantium.\n\nDeserunt voluptas placeat sed quis quis autem. Voluptatem omnis impedit temporibus explicabo esse provident. Consequuntur iste sed quis ut iure.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(50, 10, 'Qui quis error et pariatur exercitationem voluptatem.', 'Doloremque ipsum nostrum ducimus dolorum possimus recusandae. Non vel aut corporis blanditiis. Deleniti dolores ullam culpa temporibus numquam. In non ut quae exercitationem.\n\nAd voluptatum vel dignissimos quia maiores. Molestiae doloribus voluptatibus voluptas occaecati veniam. Velit eum similique neque non explicabo.\n\nVoluptatum praesentium et ut veniam commodi qui provident. Eaque aliquid rem et id. Est ad voluptatibus ut voluptatem et nisi est. Enim nostrum qui veritatis iure perspiciatis aut voluptas minus.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(51, 11, 'Ea ducimus accusamus mollitia pariatur.', 'Voluptatibus expedita provident rerum doloremque ex aut sint quos. Optio magnam repellat et molestiae officia est. Occaecati consequatur eligendi pariatur neque cumque similique.\n\nEnim ea veniam dignissimos. Et ut atque amet. Totam asperiores nostrum est ratione sit. Porro quam non sit similique quis sint.\n\nConsequatur voluptas omnis enim distinctio. Eum autem ut dolores blanditiis porro. Nesciunt quia perferendis est laborum iure cupiditate. Distinctio vero minima molestiae qui.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(52, 11, 'Molestiae eum totam corrupti harum id natus fugit.', 'Recusandae labore quae id sint repudiandae. Repudiandae ipsam et praesentium quia qui. Omnis possimus nulla eveniet culpa dolor similique sit.\n\nNeque accusantium rerum quia quia et. Perspiciatis id reiciendis odit possimus praesentium. Veniam ea illo quo.\n\nEt nobis perspiciatis et. Non repellendus nihil sunt nisi fuga quis facere repudiandae. Aut quia quis qui omnis et.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(53, 11, 'Aut voluptatem qui deleniti totam.', 'Natus perferendis labore voluptate nostrum. Ipsam amet repudiandae et suscipit. Ut omnis distinctio quia.\n\nVoluptatem illum suscipit doloribus fuga inventore in. Ad doloremque fugiat autem inventore rerum illum et ea. Est velit aut dolorem illum.\n\nNeque animi perspiciatis suscipit necessitatibus laboriosam. Molestiae sint quis qui quos laborum sunt. Ut voluptatem nulla illo assumenda quo repellendus. Error deserunt ullam ut delectus non.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(54, 11, 'Animi facere incidunt aut eligendi assumenda assumenda.', 'Nisi qui non nulla voluptatibus. Assumenda laborum iusto autem consequuntur dolore minima. Nam ab similique eum nihil voluptas temporibus quas. Possimus consequatur et voluptas nulla quod.\n\nLabore accusamus quo quia omnis vitae aut alias. Esse sunt qui voluptas atque atque tempora quam. Illum laudantium dolorum minima beatae ut. Repellat tempore libero nobis nihil nam doloremque tempore.\n\nUllam voluptas et dicta aut. Aliquam dicta exercitationem alias. Illum aut voluptas dolores perspiciatis non dolorem enim. Fugit sequi tempore doloremque mollitia et quam.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(55, 11, 'Molestias rerum similique exercitationem animi sit et eveniet.', 'Quia aspernatur qui quis in in deleniti ad culpa. Exercitationem voluptates et natus facilis modi repellendus. Aspernatur eligendi rem enim et perspiciatis. Ipsum aut molestiae nostrum corrupti.\n\nSit ex blanditiis natus recusandae. Quisquam nihil fugit quis impedit aliquam non. Alias velit fugit eum repellat. Est blanditiis ullam voluptatibus occaecati saepe omnis et omnis.\n\nNulla neque nesciunt ea ut. Numquam repudiandae repellendus eum qui aliquam. Adipisci ea fugit repellendus nulla. Ex excepturi velit quis architecto non officiis est.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(56, 12, 'Voluptas distinctio numquam ex qui nesciunt ut sit.', 'Ea deleniti reiciendis et. Ipsa harum et occaecati quam voluptates voluptas veritatis. Molestias laboriosam nam ipsum dignissimos neque. Et quasi est et ducimus libero.\n\nOmnis eligendi consequuntur alias nostrum. Maiores quo dignissimos et sed possimus. Possimus omnis delectus veniam quam ipsum cumque. Iste quo ad ut error esse ut nulla.\n\nNumquam molestiae aperiam placeat eveniet facere illum non. Repellat ab qui illo sunt ducimus. Impedit quo exercitationem sequi voluptas doloremque dolor.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(57, 12, 'Nam aut autem id non est.', 'Sunt quam et et iste. Ducimus voluptas rerum rerum excepturi dolorem nobis. Quo aut corporis blanditiis tenetur. Quae non eos cumque id eius.\n\nOccaecati et cupiditate fugit qui. Iusto et vel optio dolor necessitatibus omnis. At doloribus iusto inventore.\n\nNemo officia nam corrupti. Eaque nulla laborum iste quibusdam tenetur rerum asperiores.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(58, 12, 'Sunt provident a aspernatur delectus est.', 'Aspernatur molestiae quasi incidunt. Est cumque voluptas accusantium hic.\n\nPariatur eum qui sit ut nostrum. Beatae ad iure qui ea omnis. Voluptatem nobis suscipit voluptas. Omnis et optio neque eaque nisi laboriosam quasi et.\n\nNihil minima delectus unde nemo tempora. Fugiat et est nihil rerum ratione et soluta hic. Voluptas iure aut commodi et.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(59, 12, 'Quod eveniet deleniti debitis veritatis voluptatem nemo.', 'Incidunt sapiente vel aliquam harum. Vel facere est laudantium ut quo tempore. Ipsa deserunt qui asperiores.\n\nSint non in quisquam incidunt. Repudiandae quo modi aut et rem. Id molestiae facilis quis cumque voluptatem excepturi eum.\n\nDicta tempora sit voluptas optio facilis veritatis. Dicta suscipit repellat veritatis. Laudantium et autem voluptas totam cupiditate vel. Molestiae hic sint qui qui facilis consequatur saepe iusto.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(60, 12, 'Dolorum et aut aliquid distinctio aut atque est.', 'Voluptas dignissimos non ea quibusdam occaecati dolore. Iure numquam unde consequatur facere enim quo. Dignissimos non fugiat omnis repellendus sed deserunt.\n\nEos voluptatem ut vel quia sit at. Repudiandae et explicabo omnis molestias. Recusandae et nulla consequatur cumque voluptatem. Eaque eius aut quae sunt.\n\nMollitia soluta pariatur dolorem omnis laboriosam. Esse deserunt voluptates corrupti quis. Molestiae similique ea alias recusandae ratione rerum suscipit consequatur.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(61, 13, 'Explicabo similique cupiditate nam quis voluptatem.', 'Quasi est aut natus dolorum. Numquam consectetur maxime officiis ipsa id culpa ut. Quisquam eius quam provident at. Quod nihil rerum odit voluptas aspernatur.\n\nQuae hic et rerum. Reprehenderit minus mollitia asperiores occaecati temporibus nihil. Quo ipsum fugit voluptas maiores. Vel ea voluptatem enim est. Aut autem voluptatem officia sunt omnis sit.\n\nIste consequatur et corporis est. Ut voluptatem consectetur facilis inventore dolor modi eaque magnam. Sit non quaerat laborum nihil autem itaque illo.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(62, 13, 'Accusamus molestiae voluptas et dolor itaque numquam iste.', 'Nisi quae dolores omnis hic. Rerum aut quasi molestiae odio. Voluptates eligendi inventore pariatur fugit alias non. Cum voluptatem architecto sed fugit.\n\nNumquam vel perspiciatis et perferendis maiores eos aut. Dolorem neque error nostrum. Tempora ea id omnis corporis eius facilis rerum. Blanditiis et et omnis corporis ut sed.\n\nAut maxime corrupti et modi. Ut eos dicta dolores reiciendis. Quia vero rerum tempore dolorum magnam magni possimus voluptas. Praesentium ab dolore placeat qui sint maiores laboriosam.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(63, 13, 'Optio porro recusandae provident ut.', 'Sed repellendus veritatis ut perspiciatis dolorum eum sed. Mollitia placeat in at earum rerum quidem rem.\n\nUt id quae quo porro. Voluptatem rerum quis debitis.\n\nAccusamus eius aut dolorem. Eligendi facere sint et in et placeat quidem. Occaecati blanditiis in vero ea.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(64, 13, 'Voluptatem iste quidem veritatis dignissimos ea autem magni sit.', 'Sequi quo voluptatibus vel suscipit aut. Repudiandae omnis asperiores atque consequatur. Aut blanditiis facere sint dolor placeat qui.\n\nVero consequuntur ut tenetur vero et sed. Aut repellat dignissimos dolores. Saepe voluptatum eveniet qui vero iusto velit.\n\nAliquid omnis aspernatur cumque totam earum. Magni maiores culpa tempora iste est saepe voluptatem. Nihil iusto vero cumque numquam quia et iure eos.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(65, 13, 'Eos qui sint facilis consectetur autem.', 'Debitis consequatur dolore blanditiis non voluptas incidunt animi. Enim eaque omnis a hic. Voluptas animi ducimus nemo quia quis. Omnis ratione praesentium perspiciatis dolore.\n\nBeatae amet dolor maxime ipsa. Ratione odit quia omnis nihil. Perferendis corrupti et dicta consectetur ea.\n\nIusto tempore nemo aut minima dolorem reiciendis. Natus tempore voluptatem quibusdam libero quia et qui. Ratione qui libero maiores aut fuga. Omnis ab velit ducimus aut.', '2020-12-16 00:04:37', '2020-12-16 00:04:37'),
(66, 22, 'Building a GraphQL Server with Laravel', 'In case you\'re not currently familiar with it, GraphQL is a query language used to interact with your API...', '2020-12-16 05:42:49', '2020-12-16 05:42:49'),
(67, 24, 'Building a GraphQL Server with Laravel', 'In case you\'re not currently familiar with it, GraphQL is a query language used to interact with your API...', '2020-12-16 06:14:00', '2020-12-16 06:14:00');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('QUEUED','PROCESSING','COMPLETE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'QUEUED',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `user_id`, `title`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Eveniet vero possimus ipsa et inventore est.', 'QUEUED', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(2, 1, 'Provident est repellat asperiores velit fuga.', 'QUEUED', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(3, 1, 'Excepturi nobis exercitationem eos nemo libero.', 'QUEUED', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(4, 1, 'Et perspiciatis accusantium aspernatur veritatis qui sed.', 'PROCESSING', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(5, 1, 'Et quis qui aut sint qui nemo.', 'COMPLETE', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(6, 2, 'Enim cupiditate neque tenetur qui dignissimos.', 'QUEUED', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(7, 2, 'Atque officia voluptatum iure nobis.', 'QUEUED', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(8, 2, 'Aperiam dignissimos est minima assumenda.', 'QUEUED', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(9, 2, 'Sed atque omnis aut nihil soluta omnis et natus.', 'PROCESSING', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(10, 2, 'Soluta laudantium voluptate consequatur omnis.', 'COMPLETE', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(11, 3, 'Quae ut rerum sed temporibus ut.', 'QUEUED', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(12, 3, 'Nisi unde mollitia ducimus cupiditate molestias non consequatur.', 'QUEUED', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(13, 3, 'Qui mollitia qui quasi at nemo molestiae.', 'QUEUED', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(14, 3, 'Totam est in quis et qui aut.', 'PROCESSING', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(15, 3, 'Dolor enim magnam quis atque voluptatem ut nam.', 'COMPLETE', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(16, 4, 'Placeat sit omnis inventore ipsa porro.', 'QUEUED', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(17, 4, 'Sunt est et ut cumque quis ut consequatur minus.', 'QUEUED', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(18, 4, 'Ducimus dolor maxime consequatur iusto harum.', 'QUEUED', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(19, 4, 'Ad excepturi nostrum at esse non.', 'PROCESSING', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(20, 4, 'Tempora inventore corporis enim est dolorum non nihil ab.', 'COMPLETE', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(21, 5, 'Et libero iure consequatur nostrum.', 'QUEUED', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(22, 5, 'In adipisci magni quo aliquid debitis quia.', 'QUEUED', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(23, 5, 'Odio aperiam esse id at natus quos.', 'QUEUED', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(24, 5, 'Et omnis quia et est est nisi non.', 'PROCESSING', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(25, 5, 'Blanditiis aspernatur rerum dicta suscipit ipsam.', 'COMPLETE', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(26, 6, 'Error possimus voluptas porro illum maiores aut.', 'QUEUED', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(27, 6, 'Consequuntur voluptates iste velit et cum ex.', 'QUEUED', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(28, 6, 'Quia nostrum ea quia quidem est corporis sint distinctio.', 'QUEUED', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(29, 6, 'Numquam et placeat ipsam autem officiis.', 'PROCESSING', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(30, 6, 'Dolor aperiam aperiam error veniam est.', 'COMPLETE', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(31, 7, 'Ab mollitia autem quibusdam nostrum est at natus provident.', 'QUEUED', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(32, 7, 'Modi in sed aliquam eos ducimus ut repudiandae.', 'QUEUED', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(33, 7, 'Magni illo perspiciatis quo modi.', 'QUEUED', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(34, 7, 'Non sequi aut aut ad minus reprehenderit iusto.', 'PROCESSING', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(35, 7, 'Illo sed quo minima ducimus suscipit magnam.', 'COMPLETE', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(36, 8, 'Similique at similique delectus vel nulla voluptates illum.', 'QUEUED', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(37, 8, 'Vitae illo minima cumque laborum ut.', 'QUEUED', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(38, 8, 'Quas et in exercitationem repudiandae velit quas.', 'QUEUED', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(39, 8, 'Quae eligendi a vitae architecto delectus.', 'PROCESSING', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(40, 8, 'Consequatur accusantium molestias est excepturi consectetur sit facere cum.', 'COMPLETE', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(41, 9, 'Vel cupiditate qui aspernatur quam aliquid occaecati.', 'QUEUED', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(42, 9, 'Vero amet ut accusantium neque sit.', 'QUEUED', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(43, 9, 'Iste accusantium eaque accusamus exercitationem nihil vitae molestiae.', 'QUEUED', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(44, 9, 'Dignissimos qui quod dignissimos id asperiores.', 'PROCESSING', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(45, 9, 'Qui nihil est in suscipit minima aut.', 'COMPLETE', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(46, 10, 'Et expedita earum sit autem quibusdam voluptatem.', 'QUEUED', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(47, 10, 'Qui et veniam sed et et et vitae.', 'QUEUED', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(48, 10, 'Eum minima qui nemo rerum.', 'QUEUED', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(49, 10, 'Modi corporis dolor amet in.', 'PROCESSING', '2020-12-15 06:54:30', '2020-12-15 06:54:30'),
(50, 10, 'Qui quia neque illum a ut.', 'COMPLETE', '2020-12-15 06:54:30', '2020-12-15 06:54:30');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(13, '2014_10_12_000000_create_users_table', 1),
(14, '2014_10_12_100000_create_password_resets_table', 1),
(15, '2019_08_19_000000_create_failed_jobs_table', 1),
(16, '2020_12_14_124326_create_jobs_table', 1),
(17, '2020_12_15_122043_add_api_token_to_users_table', 1),
(18, '2020_12_16_052434_create_articles_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `api_token` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `api_token`) VALUES
(1, 'Antonette Boyer Jr.', 'nbarrows@example.org', '2020-12-15 06:54:20', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'aZFcOlMiK5', '2020-12-15 06:54:20', '2020-12-15 06:54:20', NULL),
(2, 'Anna Bernier', 'metz.kenyatta@example.net', '2020-12-15 06:54:20', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'UMIyhatOOr', '2020-12-15 06:54:20', '2020-12-15 06:54:20', NULL),
(3, 'Dr. Sonny Turcotte Sr.', 'felipe67@example.org', '2020-12-15 06:54:20', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'b827utJ0Eo', '2020-12-15 06:54:20', '2020-12-15 06:54:20', NULL),
(4, 'Prof. Tyrel Senger', 'casandra42@example.org', '2020-12-15 06:54:20', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'kavgIItgTE', '2020-12-15 06:54:20', '2020-12-15 06:54:20', NULL),
(5, 'Mr. Darryl McDermott MD', 'florencio.rowe@example.net', '2020-12-15 06:54:20', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'MxMTqPdBz9', '2020-12-15 06:54:20', '2020-12-15 06:54:20', NULL),
(6, 'Garret Emmerich', 'fbreitenberg@example.com', '2020-12-15 06:54:20', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'JBmUbklwc0', '2020-12-15 06:54:20', '2020-12-15 06:54:20', NULL),
(7, 'Prof. Johnson Bernhard', 'angel.bednar@example.org', '2020-12-15 06:54:20', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'VSvFrX5wWF', '2020-12-15 06:54:20', '2020-12-15 06:54:20', NULL),
(8, 'Berta Volkman', 'nestor09@example.org', '2020-12-15 06:54:20', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'MrMJ9O6D3Q', '2020-12-15 06:54:20', '2020-12-15 06:54:20', NULL),
(9, 'Dr. Ettie Armstrong', 'aglae29@example.net', '2020-12-15 06:54:20', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '5N8eGOKNEC', '2020-12-15 06:54:20', '2020-12-15 06:54:20', NULL),
(10, 'Mallory Johnson', 'qbauch@example.net', '2020-12-15 06:54:20', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'gsjnLeXe04', '2020-12-15 06:54:20', '2020-12-15 06:54:20', NULL),
(11, 'Jose Fonseca', 'myemail@email.com', '2020-12-15 12:46:57', '$2y$10$ClsNGQBcvN.1z5Ydpadiu.f1Al5U3.CrROVI.6a8gK99aha4o4BP.', 'MxMTqPdBz8', '2020-12-15 07:13:20', '2020-12-15 08:09:50', 'OBu7mUaeIf2Jbasc21fdBVI9uV9keqG35sU5COuSw1CCoDsXyqmXK2GfhuEx'),
(12, 'harsh vegad', 'harshvegad33@gmail.com', NULL, '$2y$10$Sh0PLNJnH/1dJxqK2e4azu5U6DohQIsmlGhutZ0Tn1tcXzNDbbIsW', NULL, '2020-12-15 08:18:04', '2020-12-15 08:18:04', NULL),
(13, 'harsh vegad', 'harshvegad3@gmail.com', NULL, '$2y$10$6d9BtMYTlrEIO.FVnak/vOJ4LK9NhZhtl1o352ZNtDfB1JGngMbMy', NULL, '2020-12-15 08:29:07', '2020-12-15 08:29:07', '20uvfvzQVseQGpe6C1m5xFyIn3znnhIdpnAQ8AD3qe62vleEhtabcDGxwTae'),
(22, 'John Doe', 'john.doe@example.com', NULL, '$2y$10$NuXDWFnF.Oo5E.SbU3kvmOqMRBs2AR13h.6hXtnwY1FXDRRG5MShG', NULL, '2020-12-16 02:12:35', '2020-12-16 05:40:20', 'IABWEFGV0KmaOc5UnAlq0SQmJjY2a1BGBCihztuTWXDmojYwKZLFZFR8KR4X'),
(23, 'John1 Doe', 'john1.doe@example.com', NULL, '$2y$10$N6sEdB6yhsSIf4LSfYKY4u5tOyxdZCsFWSrBbiitkOr4qgX5t0LLa', NULL, '2020-12-16 02:13:42', '2020-12-16 02:15:14', 'AbcAmanAj9W6OQ23b2dkmVxy4nyM1woXnmE09CBPSM2FtkyhjG5od8uRPGDo'),
(24, 'Harsh Vegad', 'harshvegad@gmail.com', NULL, '$2y$10$qoHCwBA5fVu.kf7Jb9cDn.MpiC.LJFzIk99KoO0wS/sz2srxevUkK', NULL, '2020-12-16 06:04:48', '2020-12-16 06:09:36', '1Tt7T7S1SViXaQtCkC5QQFv7rPktJ3K6mynWsGu3P94rnrwXcpQqUPtseHco');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `articles_user_id_foreign` (`user_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_user_id_foreign` (`user_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_api_token_unique` (`api_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `articles`
--
ALTER TABLE `articles`
  ADD CONSTRAINT `articles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `jobs`
--
ALTER TABLE `jobs`
  ADD CONSTRAINT `jobs_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
