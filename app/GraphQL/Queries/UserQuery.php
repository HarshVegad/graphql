<?php

namespace App\GraphQL\Queries;

use App\Models\User;
use Illuminate\Support\Facades\Auth;

class UserQuery 
{
   

    public function all()
    {
        return User::all();
    }

    public function find($reset, array $args)
    {
        return User::find($args['id']);
    }
    public function paginate($reset, array $args)
    {
        return User::query()->paginate($args['count'],['*'],'page',$args['page']);
    }
    
    public function me()
    {
        return Auth::guard('api')->user();
    }

    
}