<?php

namespace App\GraphQL\Mutations;
use Illuminate\Support\Str;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

use App\Models\User;

class UserMutation
{

    public function create($root, array $argc)
    
    {

            $token = Str::random(60);
            $user =  new User($argc);
            $user->api_token = $token;
            $user->save();

            return[
                'user' => $user,
                'token' => $token
            ];
        }
}