<?php

namespace App\GraphQL\Mutations;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;


class AuthMutation
{

    public function login($root, array $argc)
    {
        $credentials = Arr::only($argc,['email', 'password']);
        if(Auth::once($credentials))
        {
            $token = Str::random(60);
            $user = auth()->user();
            $user->api_token = $token;
            $user->save();
            return $token;
        }
        return null;
    }

     public function resolve($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $credentials = Arr::only($args, ['email', 'password']);

        if (Auth::once($credentials)) {
            $token = Str::random(60);

            $user = auth()->user();
            $user->api_token = $token;
            $user->save();

            return $token;
        }

        return null;
    }
}


